﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AjaxCRUD.Models;
using AjaxCRUD.Models.ProjectEntities;

namespace AjaxCRUD.Controllers
{
    public class EmployeeController : Controller
    {
        private ApplicationDbContext _context;

        public EmployeeController()
        {
            _context=new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Employee
        public ActionResult Index()
        {
           
            return View();
        }

        public ActionResult ViewAll()
        {
           // var employees = _context.Employees.ToList();
            return View(GetAllEmployee());
        }


        //Incase error will use this method for casting
        IEnumerable<Employee> GetAllEmployee()
        {
            return _context.Employees.ToList<Employee>();
        }

        public ActionResult AddOrEdit(int id=0)
        {
            var employee= new Employee();
            return View(employee);
        }

        [HttpPost]
        public ActionResult AddOrEdit(Employee employee)
        {
            //if any image is selected it will be uploaded to the image folder
            try
            {
                if (employee.ImageUpload != null)
                {
                    var fileName = Path.GetFileNameWithoutExtension(employee.ImageUpload.FileName);
                    var extension = Path.GetExtension(employee.ImageUpload.FileName);
                    fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                    //Update the property
                    employee.ImagePath = "~/AppFiles/Image/" + fileName;
                    //Saving the image to the image folder
                    employee.ImageUpload.SaveAs(Path.Combine(Server.MapPath("~/AppFiles/Image/"), fileName));
                }
                //save to the employee table the data 
                //for security reason use Manual EmployeInDB method

                using (_context)
                {
                    _context.Employees.Add(employee);
                    _context.SaveChanges();
                }

                // Will return JSON Data return RedirectToAction("Index");
                return Json(new { success = true, html = GlobalClass.RenderRazorViewToString(this, "ViewAll", GetAllEmployee()), message = "Submitted Successfully" }, JsonRequestBehavior.AllowGet);

            }
            
            //incase any error
            catch (Exception ex)
            {
                
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);

            }
        }


    }
}