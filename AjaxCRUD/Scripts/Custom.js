﻿
//Image Event
function ShowImagePreview(imageUploader, previewImage) {
    //check if we have image for upload or not
    if (imageUploader.files && imageUploader.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $(previewImage).attr("src", e.target.result);
        }
        reader.readAsDataURL(imageUploader.files[0]);
    }
}

function jQueryAjaxPost(form) {

    $.validator.unobtrusive.parse(form);
    if ($(form).valid()) {

        //Configuring function for all 
        var ajaxConfig =
        {
            type: 'POST',
            url: form.action,
            data: new formData(form),
            //if the operation is successful we can do the followings else
              success: function (response) {

                  $("#firstTab").html(response);
                  //call here the refresh function
                  refreshAddNewTab($(form).attr('data-restUrl'), true);
                  //success Message here
            }
            //Or if the operation is not successful 
            else {
            //error Message here
        }

        }

        if ($(form).attr('enctype') === "multipart/form-data") {
                ajaxConfig["contentType"] = false;
                ajaxConfig["processData"] = false;

        }

        $.ajax(ajaxConfig);
    }
}

//for the refresh operation
function refreshAddNewTab(resetUrl, showViewTab) {
    $.ajax({
        type: 'GET',
        url: resetUrl,
        success: function(response) {
            $("#secondTab").html(response);
            $('ul.nav.nav-tabs a:eq(1)').html('Add New');

            if (showViewTab)
                $('ul.nav.nav-tabs a:eq(0)').tab('show');

        }

    });
}