﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace AjaxCRUD.Models.ProjectEntities
{
    [Table("Employee")]
    public class Employee
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required (ErrorMessage = "Cant be empty")]
        public string Name { get; set; }
        public string Position { get; set; }
        public string Office { get; set; }
        public int Salary { get; set; }

        [DisplayName("Image")]
        public string ImagePath { get; set; }

        //Not in the model
        [NotMapped]
        public HttpPostedFileBase ImageUpload { get; set; }

        //constructor for the default image path

        public Employee()
        {
            ImagePath = "~/AppFiles/Image/Default.png";
        }

    }
}