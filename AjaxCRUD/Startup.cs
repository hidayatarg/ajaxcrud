﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AjaxCRUD.Startup))]
namespace AjaxCRUD
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
